﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class CylinderController : MonoBehaviour {

    GameObject RightCard;
    GameObject LeftCard;
    GameObject RightCylinder;
    GameObject LeftCylinder;

    bool LeftVisible;
    bool RightVisible;


    // Use this for initialization
    void Start () {

        RightCard = GameObject.Find("RightCardTarget");
        RightCylinder = RightCard.transform.GetChild(0).gameObject;

        LeftCard = GameObject.Find("LeftCardTarget");
        LeftCylinder = LeftCard.transform.GetChild(0).gameObject;
	}

    void ChkVisible()
    {
        if (RightCard.GetComponent<ImageTargetBehaviour>().CurrentStatus == TrackableBehaviour.Status.TRACKED)
        {
            RightVisible = true;
        }
        else if (RightCard.GetComponent<ImageTargetBehaviour>().CurrentStatus != TrackableBehaviour.Status.TRACKED)
        {
            RightVisible = false;
        }

        if (LeftCard.GetComponent<ImageTargetBehaviour>().CurrentStatus == TrackableBehaviour.Status.TRACKED)
        {
            LeftVisible = true;
        }
        else if (LeftCard.GetComponent<ImageTargetBehaviour>().CurrentStatus != TrackableBehaviour.Status.TRACKED)
        {
            LeftVisible = false;
        }
    }

    void AdjustVisible()
    {
        if(RightVisible && LeftVisible)
        {
            RightCylinder.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
            LeftCylinder.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
        }
        else if (RightVisible)
        {
            RightCylinder.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
        }
        else if (LeftVisible)
        {
            LeftCylinder.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
        }
    }

    // Update is called once per frame
    void Update () {
        

        ChkVisible();

        AdjustVisible();
    }
        

}
